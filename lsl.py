#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  Copyright (C) 2009 Filippo Baruffaldi
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "LSL Pygments Lexer"), to
# deal in LSL Pygments Lexer without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of LSL Pygments Lexer, and to permit persons to whom LSL Pygments Lexer is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: Coolminds Developers Network
@copyright: Filippo Baruffaldi
@license: MIT License
"""

from pygments.lexer import RegexLexer, bygroups, include, combined
from pygments.token import *

class LSLLexer(RegexLexer):
    name = 'LSL'
    aliases = ['lsl']
    filenames = ['*.lsl']

    tokens = {
        'root': [
            (r'[ ]+', Whitespace),

            (r'/\*', Comment.Multiline, 'comment'),
            (r'//.*?$', Comment.Singleline),

            (r'(state|default)\b', Keyword.Declaration),
            include('events'),
            include('flowcontrol'),
            include('datatypes'),
            include('operators'),
            include('punctuation'),
            include('numbers'),
            include('builtins'),
            include('constants'),

            ('(?:[rR]|[uU][rR]|[rR][uU])"""', String, 'tdqs'),
            ("(?:[rR]|[uU][rR]|[rR][uU])'''", String, 'tsqs'),
            ('(?:[rR]|[uU][rR]|[rR][uU])"', String, 'dqs'),
            ("(?:[rR]|[uU][rR]|[rR][uU])'", String, 'sqs'),
            ('[uU]?"""', String, combined('stringescape', 'tdqs')),
            ("[uU]?'''", String, combined('stringescape', 'tsqs')),
            ('[uU]?"', String, combined('stringescape', 'dqs')),
            ("[uU]?'", String, combined('stringescape', 'sqs')),

            ("[a-zA-Z0-9_-]+", Name)
        ],
        'comment': [
            (r'[^*/]', Comment.Multiline),
            (r'/\*', Comment.Multiline, '#push'),
            (r'\*/', Comment.Multiline, '#pop'),
            (r'[*/]', Comment.Multiline)
        ],
        'events': [
            (r'(on_rez|state_entry|state_exit|changed|'
             r'run_time_permissions|attach|object_rez|'
             r'listen|link_message|email|http_response|'
             r'remote_data|dataserver|touch_start|'
             r'touch_end|sensor|no_sensor|touch|land_collision|'
             r'land_collision_start|land_collision_end|collision|'
             r'collision_start|collision_end|moving_start|moving_end|'
             r'control|money|at_target|not_at_target|at_rot_target|'
             r'not_at_rot_target)\b', Generic.Output),
        ],
        'numbers': [
            (r'(\d+\.\d*|\d*\.\d+)([eE][+-]?[0-9]+)?', Number.Float),
            (r'\d+[eE][+-]?[0-9]+', Number.Float),
            (r'0\d+', Number.Oct),
            (r'0[xX][a-fA-F0-9]+', Number.Hex),
            (r'\d+L', Number.Integer.Long),
            (r'\d+', Number.Integer)
        ],
        'stringescape': [
            (r'\\([\\abfnrtv"\']|\n|N{.*?}|u[a-fA-F0-9]{4}|'
             r'U[a-fA-F0-9]{8}|x[a-fA-F0-9]{2}|[0-7]{1,3})', String.Escape)
        ],
        'strings': [
            (r'%(\([a-zA-Z0-9_]+\))?[-#0 +]*([0-9]+|[*])?(\.([0-9]+|[*]))?'
             '[hlL]?[diouxXeEfFgGcrs%]', String.Interpol),
            (r'[^\\\'"%\n]+', String),
            # quotes, percents and backslashes must be parsed one at a time
            (r'[\'"\\]', String),
            # unhandled string formatting sign
            (r'%', String)
            # newlines are an error (use "nl" state)
        ],
        'nl': [
            (r'\n', String)
        ],
        'dqs': [
            (r'"', String, '#pop'),
            (r'\\\\|\\"|\\\n', String.Escape),
            include('strings')
        ],
        'sqs': [
            (r"'", String, '#pop'),
            (r"\\\\|\\'|\\\n", String.Escape),
            include('strings')
        ],
        'tdqs': [
            (r'"""', String, '#pop'),
            include('strings'),
            include('nl')
        ],
        'tsqs': [
            (r"'''", String, '#pop'),
            include('strings'),
            include('nl')
        ],
        'flowcontrol': [
            (r'\b(for|if|while|do-while|jump|return|'
             r'return|try|while|do|else)\b', Keyword),
        ],
        'operators': [
            (r'\||\!|\&|!=|==|<<|>>|[-~+/*%=<>&^|.]', Operator),
        ],
        'punctuation': [
            (r'[]{}:(),;[]', Punctuation),
        ],
        'datatypes': [
            (r'(integer|float|string|key|list|vector|rotation)([ ]+|)(\w+)\b',
             bygroups(Name.Entity, Other, Name)),
        ],
        'builtins': [
           (r'(llSin|llCos|llTan|llAtan2|llSqrt|llPow|llAbs|llFabs|'
            r'llFrand|llFloor|llCeil|llRound|llVecMag|'
            r'llVecNorm|llVecDist|llRot2Euler|llEuler2Rot|'
            r'llAxes2Rot|llRot2Fwd|llRot2Left|llRot2Up|'
            r'llRotBetween|llWhisper|llSay|llShout|'
            r'llListen|llListenControl|llListenRemove|llSensor|'
            r'llSensorRepeat|llSensorRemove|llDetectedName|llDetectedKey|'
            r'llDetectedOwner|llDetectedType|llDetectedPos|llDetectedVel|'
            r'llDetectedGrab|llDetectedRot|llDetectedGroup|llDetectedLinkNumber|'
            r'llDie|llGround|llCloud|llWind|'
            r'llSetStatus|llGetStatus|llSetScale|llGetScale|'
            r'llSetColor|llGetAlpha|llSetAlpha|'
            r'llGetColor|llSetTexture|llScaleTexture|llOffsetTexture|'
            r'llRotateTexture|llGetTexture|llSetPos|llGetPos|'
            r'llGetLocalPos|llSetRot|llGetRot|llGetLocalRot|'
            r'llSetForce|llGetForce|llTarget|llTargetRemove|'
            r'llRotTarget|llRotTargetRemove|llMoveToTarget|llStopMoveToTarget|'
            r'llApplyImpulse|llApplyRotationalImpulse|llSetTorque|llGetTorque|'
            r'llSetForceAndTorque|llGetVel|llGetAccel|llGetOmega|'
            r'llGetTimeOfDay|llGetWallclock|llGetTime|llResetTime|'
            r'llGetAndResetTime|llSound|llPlaySound|llLoopSound|'
            r'llLoopSoundMaster|llLoopSoundSlave|llPlaySoundSlave|llTriggerSound|'
            r'llStopSound|llPreloadSound|llGetSubString|llDeleteSubString|llInsertString|llToUpper|llToLower|'
            r'llGiveMoney|llMakeExplosion|llMakeFountain|llMakeSmoke|llMakeFire|llRezObject|'
            r'llLookAt|llStopLookAt|llSetTimerEvent|llSleep|llGetMass|llCollisionFilter|'
            r'llTakeControls|llReleaseControls|llAttachToAvatar|llDetachFromAvatar|llTakeCamera|'
            r'llReleaseCamera|llGetOwner|llInstantMessage|llEmail|llGetNextEmail|'
            r'llGetKey|llSetBuoyancy|llSetHoverHeight|llStopHover|llMinEventDelay|'
            r'llSoundPreload|llRotLookAt|llStringLength|llStartAnimation|llStopAnimation|'
            r'llPointAt|llStopPointAt|llTargetOmega|llGetStartParameter|llGodLikeRezObject|'
            r'llRequestPermissions|llGetPermissionsKey|llGetPermissions|llGetLinkNumber|llSetLinkColor|'
            r'llCreateLink|llBreakLink|llBreakAllLinks|llGetLinkKey|'
            r'llGetLinkName|llGetInventoryNumber|llGetInventoryName|llSetScriptState|'
            r'llGetEnergy|llGiveInventory|llRemoveInventory|llSetText|llWater|llPassTouches|'
            r'llRequestAgentData|llRequestInventoryData|llSetDamage|llTeleportAgentHome|'
            r'llModifyLand|llCollisionSound|llCollisionSprite|llGetAnimation|llResetScript|'
            r'llMessageLinked|llPushObject|llPassCollisions|llGetScriptName|'
            r'llGetNumberOfSides|llAxisAngle2Rot|llRot2Axis|llRot2Angle|llAcos|llAsin|'
            r'llAngleBetween|llGetInventoryKey|llAllowInventoryDrop|llGetSunDirection|'
            r'llGetTextureOffset|llGetTextureScale|llGetTextureRot|llSubStringIndex|'
            r'llGetOwnerKey|llGetCenterOfMass|llListSort|llGetListLength|'
            r'llList2Integer|llList2Float|llList2String|llList2Key|'
            r'llList2Vector|llList2Rot|llList2List|llDeleteSubList|'
            r'llGetListEntryType|llList2CSV|llCSV2List|llListRandomize|'
            r'llList2ListStrided|llGetRegionCorner|llListInsertList|llListFindList|'
            r'llGetObjectName|llSetObjectName|llGetDate|llEdgeOfWorld|'
            r'llGetAgentInfo|llAdjustSoundVolume|llSetSoundQueueing|llSetSoundRadius|'
            r'llKey2Name|llSetTextureAnim|llTriggerSoundLimited|llEjectFromLand|'
            r'llParseString2List|llOverMyLand|llGetLandOwnerAt|llGetNotecardLine|'
            r'llGetAgentSize|llSameGroup|llUnSit|llGroundSlope|'
            r'llGroundNormal|llGroundContour|llGetAttached|llGetFreeMemory|'
            r'llGetRegionName|llGetRegionTimeDilation|llGetRegionFPS|llParticleSystem|'
            r'llGroundRepel|llGiveInventoryList|llSetVehicleType|llSetVehicleFloatParam|'
            r'llSetVehicleVectorParam|llSetVehicleRotationParam|llSetVehicleFlags|llRemoveVehicleFlags|'
            r'llSitTarget|llAvatarOnSitTarget|llAddToLandPassList|llSetTouchText|'
            r'llSetSitText|llSetCameraEyeOffset|llSetCameraAtOffset|llDumpList2String|'
            r'llScriptDanger|llDialog|llVolumeDetect|llResetOtherScript|'
            r'llGetScriptState|llRemoteLoadScript|llSetRemoteScriptAccessPin|llRemoteLoadScriptPin|'
            r'llOpenRemoteDataChannel|llSendRemoteData|llRemoteDataReply|llCloseRemoteDataChannel|'
            r'llMD5String|llSetPrimitiveParams|llStringToBase64|llBase64ToString|'
            r'llXorBase64Strings|llRemoteDataSetRegion|llLog10|llLog|'
            r'llGetAnimationList|llSetParcelMusicURL|llGetRootPosition|llGetRootRotation|llGetObjectDesc|'
            r'llSetObjectDesc|llGetCreator|llGetTimestamp|llSetLinkAlpha|'
            r'llGetNumberOfPrims|llGetNumberOfNotecardLines|llGetBoundingBox|llGetGeometricCenter|'
            r'llGetPrimitiveParams|llIntegerToBase64|llBase64ToInteger|llGetGMTclock|'
            r'llGetSimulatorHostname|llSetLocalRot|llParseStringKeepNulls|llRezAtRoot|'
            r'llGetObjectPermMask|llSetObjectPermMask|llGetInventoryPermMask|llSetInventoryPermMask|'
            r'llGetInventoryCreator|llOwnerSay|llRequestSimulatorData|llForceMouselook|'
            r'llGetObjectMass|llListReplaceList|llLoadURL|llParcelMediaCommandList|'
            r'llParcelMediaQuery|llModPow|llGetInventoryType|llSetPayPrice|'
            r'llGetCameraPos|llGetCameraRot|llSetPrimURL|llRefreshPrimURL|'
            r'llEscapeURL|llUnescapeURL|llMapDestination|llAddToLandBanList|'
            r'llRemoveFromLandPassList|llRemoveFromLandBanList|llSetCameraParams|'
            r'llClearCameraParams|llListStatistics|llGetUnixTime|llGetParcelFlags|'
            r'llGetRegionFlags|llXorBase64StringsCorrect|llHTTPRequest|llResetLandBanList|'
            r'llResetLandPassList|llGetParcelPrimCount|llGetParcelPrimOwners|llGetObjectPrimCount|'
            r'llGetParcelMaxPrims|llGetParcelDetails|llSetLinkPrimitiveParams|llSetLinkTexture|'
            r'llStringTrim|llRegionSay|llGetObjectDetails|llSetClickAction|'
            r'llGetRegionAgentCount|llTextBox|llGetAgentLanguage|llDetectedTouchUV|'
            r'llDetectedTouchFace|llDetectedTouchPos|llDetectedTouchNormal|llDetectedTouchBinormal|'
            r'llDetectedTouchST|llSHA1String|llGetFreeURLs|llRequestURL|'
            r'llRequestSecureURL|llReleaseURL|llHTTPResponse|llGetHTTPHeader)\b', Name.Builtin),
        ],
        'constants':[
           (r'(ACTIVE|AGENT|AGENT_ALWAYS_RUN|AGENT_ATTACHMENTS|AGENT_AUTOPILOT|'
            r'AGENT_AWAY|AGENT_BUSY|AGENT_CROUCHING|'
            r'AGENT_FLYING|AGENT_IN_AIR|AGENT_MOUSELOOK|'
            r'AGENT_ON_OBJECT|AGENT_SCRIPTED|AGENT_SITTING|'
            r'AGENT_TYPING|AGENT_WALKING|ALL_SIDES|'
            r'ANIM_ON|ATTACH_BACK|ATTACH_BELLY|'
            r'ATTACH_CHEST|ATTACH_HEAD|ATTACH_HUD_BOTTOM|'
            r'ATTACH_HUD_BOTTOM_LEFT|ATTACH_HUD_CENTE|'
            r'ATTACH_HUD_TOP_CENTER|ATTACH_HUD_TOP_LEFT|ATTACH_HUD_TOP_RIGHT|'
            r'ATTACH_LEAR|ATTACH_LEFT_PEC|ATTACH_LEYE|'
            r'ATTACH_LFOOT|ATTACH_LHAND|ATTACH_LHIP|'
            r'ATTACH_LLARM|ATTACH_LLLEG|ATTACH_LSHOULDE|'
            r'ATTACH_LULEG|ATTACH_MOUTH|ATTACH_NOSE|'
            r'ATTACH_PELVIS|ATTACH_REAR|ATTACH_REYE|'
            r'ATTACH_RHAND|ATTACH_RHIP|ATTACH_RIGHT_PEC|'
            r'ATTACH_RLARM|ATTACH_RLLEG|ATTACH_RSHOULDE|'
            r'ATTACH_RUARM|ATTACH_RULEG|CAMERA_ACTIVE|'
            r'CAMERA_BEHINDNESS_ANGLE|CAMERA_BEHINDNESS_LAG|CAMERA_DISTANCE|'
            r'CAMERA_FOCUS|CAMERA_FOCUS_LAG|CAMERA_FOCUS_LOCKED|'
            r'CAMERA_FOCUS_OFFSET|CAMERA_FOCUS_THRESHOLD|CAMERA_PITCH|'
            r'CAMERA_POSITION|CAMERA_POSITION_LAG|CAMERA_POSITION_LOCKED|'
            r'CAMERA_POSITION_THRESHOLD|CHANGED_ALLOWED_DROP|CHANGED_COLO|'
            r'CHANGED_INVENTORY|CHANGED_LINK|CHANGED_MEDIA|'
            r'CHANGED_OWNER|CHANGED_REGION|CHANGED_REGION_START|'
            r'CHANGED_SCALE|CHANGED_SHAPE|CHANGED_TELEPORT|'
            r'CHANGED_TEXTURE|CLICK_ACTION_BUY|CLICK_ACTION_NONE|'
            r'CLICK_ACTION_OPEN|CLICK_ACTION_OPEN_MEDIA|CLICK_ACTION_PAY|'
            r'CLICK_ACTION_PLAY|CLICK_ACTION_SIT|CLICK_ACTION_TOUCH|'
            r'CONTROL_BACK|CONTROL_DOWN|CONTROL_FWD|'
            r'CONTROL_LBUTTON|CONTROL_LEFT|CONTROL_ML_LBUTTON|'
            r'CONTROL_RIGHT|CONTROL_ROT_LEFT|CONTROL_ROT_RIGHT|'
            r'CONTROL_UP|DATA_BORN|DATA_NAME|'
            r'DATA_ONLINE|DATA_PAYINFO|DATA_RATING|'
            r'DATA_SIM_POS|DATA_SIM_RATING|DATA_SIM_STATUS|'
            r'DEBUG_CHANNEL|FALSE|HTTP_BODY_MAXLENGTH|HTTP_BODY_TRUNCATED|'
            r'HTTP_METHOD|HTTP_MIMETYPE|HTTP_VERIFY_CERT|'
            r'INVENTORY_ALL|INVENTORY_ANIMATION|INVENTORY_BODYPART|'
            r'INVENTORY_CLOTHING|INVENTORY_GESTURE|INVENTORY_NONE|'
            r'INVENTORY_NOTECARD|INVENTORY_OBJECT|INVENTORY_SCRIPT|'
            r'INVENTORY_SOUND|INVENTORY_TEXTURE|LAND_LEVEL|'
            r'LAND_LOWER|LAND_NOISE|LAND_RAISE|'
            r'LAND_REVERT|LAND_SMOOTH|LINK_ALL_CHILDREN|'
            r'LINK_ALL_OTHERS|LINK_ROOT|LINK_SET|'
            r'LINK_THIS|LIST_STAT_GEOMETRIC_MEAN|LIST_STAT_MAX|'
            r'LIST_STAT_MEAN|LIST_STAT_MEDIAN|LIST_STAT_MIN|'
            r'LIST_STAT_NUM_COUNT|LIST_STAT_RANGE|LIST_STAT_STD_DEV|'
            r'LIST_STAT_SUM|LIST_STAT_SUM_SQUARES|LOOP|'
            r'MASK_BASE|MASK_EVERYONE|MASK_GROUP|'
            r'MASK_NEXT|MASK_OWNER|NULL_KEY|'
            r'OBJECT_CREATOR|OBJECT_DESC|OBJECT_GROUP|'
            r'OBJECT_NAME|OBJECT_OWNER|OBJECT_POS|'
            r'OBJECT_ROT|OBJECT_UNKNOWN_DETAIL|OBJECT_VELOCITY|'
            r'PARCEL_COUNT_GROUP|PARCEL_COUNT_OTHER|PARCEL_COUNT_OWNER|'
            r'PARCEL_COUNT_SELECTED|PARCEL_COUNT_TEMP|PARCEL_COUNT_TOTAL|'
            r'PARCEL_DETAILS_AREA|PARCEL_DETAILS_DESC|PARCEL_DETAILS_GROUP|'
            r'PARCEL_DETAILS_NAME|PARCEL_DETAILS_OWNER|PARCEL_FLAG_ALLOW_ALL_OBJECT_ENTRY|'
            r'PARCEL_FLAG_ALLOW_CREATE_GROUP_OBJECTS|PARCEL_FLAG_ALLOW_CREATE_OBJECTS|PARCEL_FLAG_ALLOW_DAMAGE|'
            r'PARCEL_FLAG_ALLOW_FLY|PARCEL_FLAG_ALLOW_GROUP_OBJECT_ENTRY|PARCEL_FLAG_ALLOW_GROUP_SCRIPTS|'
            r'PARCEL_FLAG_ALLOW_LANDMARK|PARCEL_FLAG_ALLOW_SCRIPTS|PARCEL_FLAG_ALLOW_TERRAFORM|'
            r'PARCEL_FLAG_LOCAL_SOUND_ONLY|PARCEL_FLAG_RESTRICT_PUSHOBJECT|PARCEL_FLAG_USE_ACCESS_GROUP|'
            r'PARCEL_FLAG_USE_BAN_LIST|PARCEL_FLAG_USE_ACCESS_LIST|'
            r'PARCEL_MEDIA_COMMAND_AGENT|PARCEL_MEDIA_COMMAND_AUTO_ALIGN|'
            r'PARCEL_MEDIA_COMMAND_DESC|PARCEL_MEDIA_COMMAND_LOOP|'
            r'PARCEL_MEDIA_COMMAND_LOOP_SET|PARCEL_MEDIA_COMMAND_PAUSE|'
            r'PARCEL_MEDIA_COMMAND_PLAY|PARCEL_MEDIA_COMMAND_SIZE|'
            r'PARCEL_MEDIA_COMMAND_STOP|PARCEL_MEDIA_COMMAND_TEXTURE|'
            r'PARCEL_MEDIA_COMMAND_TIME|PARCEL_MEDIA_COMMAND_TYPE|'
            r'PARCEL_MEDIA_COMMAND_UNLOAD|PARCEL_MEDIA_COMMAND_URL|'
            r'PAYMENT_INFO_ON_FILE|PAYMENT_INFO_USED|PAY_DEFAULT|PAY_HIDE|PERMISSION_ATTACH|'
            r'PERMISSION_CHANGE_LINKS|PERMISSION_CONTROL_CAMERA|'
            r'PERMISSION_DEBIT|PERMISSION_TAKE_CONTROLS|'
            r'PERMISSION_TRACK_CAMERA|PERMISSION_TRIGGER_ANIMATION|'
            r'PERM_ALL|PERM_COPY|PERM_MODIFY|PERM_TRANSFER|PING_PONG|'
            r'PRIM_BUMP_BARK|PRIM_BUMP_BLOBS|PRIM_BUMP_BRICKS|'
            r'PRIM_BUMP_BRIGHT|PRIM_BUMP_CHECKER|'
            r'PRIM_BUMP_CONCRETE|PRIM_BUMP_DARK|'
            r'PRIM_BUMP_DISKS|PRIM_BUMP_GRAVEL|'
            r'PRIM_BUMP_LARGETILE|PRIM_BUMP_NONE|'
            r'PRIM_BUMP_SHINY|PRIM_BUMP_SIDING|'
            r'PRIM_BUMP_STONE|PRIM_BUMP_STUCCO|'
            r'PRIM_BUMP_SUCTION|PRIM_BUMP_TILE|'
            r'PRIM_BUMP_WEAVE|PRIM_BUMP_WOOD|'
            r'PRIM_COLOR|PRIM_FLEXIBLE|'
            r'PRIM_FULLBRIGHT|PRIM_GLOW|'
            r'PRIM_HOLE_CIRCLE|PRIM_HOLE_DEFAULT|'
            r'PRIM_HOLE_SQUARE|PRIM_HOLE_TRIANGLE|'
            r'PRIM_MATERIAL|PRIM_MATERIAL_GLASS|'
            r'PRIM_MATERIAL_LIGHT|PRIM_MATERIAL_METAL|'
            r'PRIM_MATERIAL_PLASTIC|PRIM_MATERIAL_RUBBER|'
            r'PRIM_MATERIAL_STONE|PARCEL_FLAG_USE_LAND_PASS_LIST|'
            r'PRIM_MATERIAL_WOOD|PRIM_PHANTOM|'
            r'PRIM_PHYSICS|PRIM_POINT_LIGHT|'
            r'PRIM_POSITION|PRIM_ROTATION|'
            r'PRIM_SCULPT_FLAG_INVERT|PRIM_SCULPT_FLAG_MIRROR|'
            r'PRIM_SCULPT_TYPE_CYLINDER|PRIM_SCULPT_TYPE_MASK|'
            r'PRIM_SCULPT_TYPE_PLANE|PRIM_SCULPT_TYPE_SPHERE|'
            r'PRIM_SCULPT_TYPE_TORUS|PRIM_SHINY_HIGH|'
            r'PRIM_SHINY_LOW|PRIM_SHINY_MEDIUM|'
            r'PRIM_SHINY_NONE|PRIM_SIZE|'
            r'PRIM_TEMP_ON_REZ|PRIM_TEXGEN|'
            r'PRIM_TEXGEN_DEFAULT|PRIM_TEXGEN_PLANAR|'
            r'PRIM_TEXTURE|PRIM_TYPE|'
            r'PRIM_TYPE_BOX|PRIM_TYPE_CYLINDER|PRIM_TYPE_PRISM|'
            r'PRIM_TYPE_RING|PRIM_TYPE_SCULPT|'
            r'PRIM_TYPE_SPHERE|PRIM_TYPE_TORUS|PUBLIC_CHANNEL|'
            r'REGION_FLAG_ALLOW_DAMAGE|REGION_FLAG_ALLOW_DIRECT_TELEPORT|'
            r'REGION_FLAG_BLOCK_FLY|REGION_FLAG_BLOCK_TERRAFORM|'
            r'REGION_FLAG_DISABLE_COLLISIONS|'
            r'REGION_FLAG_DISABLE_PHYSICS|REGION_FLAG_FIXED_SUN|'
            r'REGION_FLAG_RESTRICT_PUSHOBJECT|REGION_FLAG_SANDBOX|'
            r'REMOTE_DATA_CHANNEL|REMOTE_DATA_REPLY|'
            r'REMOTE_DATA_REQUEST|REVERSE|'
            r'ROTATE|SCALE|SCRIPTED|SMOOTH|STATUS_BLOCK_GRAB|'
            r'STATUS_CAST_SHADOWS|STATUS_DIE_AT_EDGE|'
            r'STATUS_PHANTOM|STATUS_PHYSICS|'
            r'STATUS_RETURN_AT_EDGE|STATUS_ROTATE_X|'
            r'STATUS_ROTATE_Y|STATUS_ROTATE_Z|'
            r'STATUS_SANDBOX|STRING_TRIM|'
            r'STRING_TRIM_HEAD|STRING_TRIM_TAIL|TOUCH_INVALID_FACE|'
            r'TOUCH_INVALID_TEXCOORD|TOUCH_INVALID_VECTOR|TRUE|'
            r'TYPE_FLOAT|TYPE_INTEGER|TYPE_INVALID|TYPE_KEY|'
            r'TYPE_ROTATION|TYPE_STRING|TYPE_VECTOR|VEHICLE_ANGULAR_DEFLECTION_EFFICIENCY|'
            r'VEHICLE_ANGULAR_FRICTION_TIMESCALE|VEHICLE_ANGULAR_MOTOR_DECAY_TIMESCALE|'
            r'VEHICLE_ANGULAR_MOTOR_DIRECTION|VEHICLE_ANGULAR_MOTOR_TIMESCALE|'
            r'VEHICLE_BANKING_EFFICIENCY|VEHICLE_BANKING_MIX|'
            r'VEHICLE_BANKING_TIMESCALE|VEHICLE_BUOYANCY|'
            r'VEHICLE_FLAG_CAMERA_DECOUPLED|VEHICLE_FLAG_HOVER_GLOBAL_HEIGHT|'
            r'VEHICLE_FLAG_HOVER_TERRAIN_ONLY|VEHICLE_FLAG_HOVER_UP_ONLY|'
            r'VEHICLE_FLAG_HOVER_WATER_ONLY|VEHICLE_FLAG_LIMIT_MOTOR_UP|'
            r'VEHICLE_FLAG_LIMIT_ROLL_ONLY|VEHICLE_FLAG_MOUSELOOK_BANK|'
            r'VEHICLE_FLAG_MOUSELOOK_STEER|VEHICLE_FLAG_NO_DEFLECTION_UP|'
            r'VEHICLE_HOVER_EFFICIENCY|VEHICLE_HOVER_HEIGHT|'
            r'VEHICLE_HOVER_TIMESCALE|VEHICLE_LINEAR_DEFLECTION_EFFICIENCY|'
            r'VEHICLE_LINEAR_DEFLECTION_TIMESCALE|VEHICLE_LINEAR_FRICTION_TIMESCALE|ZERO_VECTOR)\b', Name.Constant),
            ( r'\b(TEXTURE_DEFAULT|TEXTURE_BLANK|TEXTURE_MEDIA|TEXTURE_PLYWOOD|TEXTURE_TRANSPARENT|URL_REQUEST_DENIED|URL_REQUEST_GRANTED|EOF|NULL_KEY)\b', Name.Label),
            ( r'(TWO_PI|SQRT2|RAD_TO_DEG|DEG_TO_RAD|PI|PI_BY_TWO)', Name.Tag),
        ],
    }